
const userTypedefs = `
type User{
    name:String
    gender:String
    age:Int
    email:String
    password:String
}
type Restaurant{
    rest_name:String
    rest_address:String
    rest_city:String
}

type Query{
    getUsers:[User]  
    getRestaurants:[Restaurant] 
}
type Mutation{
    createUser(name: String!,age: Int!,gender: String!,email: String!,password: String!):User
    loginUser(email: String!,password: String!):User
    updateUser(id:ID!,name: String,age: Int,gender: String,email: String,password: String):User
    deleteUser(email:String!):User
    createRestaurant(rest_name:String!,rest_address:String!,rest_city:String!):Restaurant
    updateRestaurant(id:ID!,rest_name:String,rest_address:String,rest_city:String):Restaurant
    deleteRestaurant(id:ID!):Restaurant
}
`

module.exports = userTypedefs