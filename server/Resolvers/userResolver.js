const UserSchema = require("../Models/userSchema");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const userSchema = require("../Models/userSchema");
const RestaurentSchema = require("../Models/RestaurentSchema")
SECRET_KEY = "RESTAURENTAPI";

const userResolver = {
  Query: {
    getUsers: async () => {
      try {
        const users = await UserSchema.find();
        return users;
      } catch (error) {
        console.error("Error fetching users:", error);
        throw new Error("Failed to fetch users.");
      }
    },
    getRestaurants: async () => {
      try {
        const getrestdata = await RestaurentSchema.find();
        return getrestdata;
      } catch (error) {
        console.error("Error fetching Restaurants:", error);
        throw new Error("Failed to fetch Restaurants.");
      }
    },
  },
  Mutation: {
    createUser: async (parent, args) => {
      const { name, age, gender, email } = args;
      try {
        const existingUser = await UserSchema.findOne({ email });
        if (existingUser) {
          throw new Error("This email is already in use.");
        }

        //hash password
        const hashedPassword = await bcrypt.hash(args.password, 12);

        const user = new UserSchema({
          name,
          age,
          gender,
          email,
          password: hashedPassword,
        });
        await user.save();

        //create token
        const token = jwt.sign({ email: user.email, id: user._id }, SECRET_KEY);

        const { password, ...rest } = user.toObject(); // Convert to object to exclude mongoose-specific properties
        const displayData = { ...rest, token };

        return displayData;
      } catch (error) {
        console.error("Error creating user:", error);
        throw new Error("Failed to create user.");
      }
    },
    loginUser: async (parent, args) => {
      const { email, password } = args;
      try {
        const user = await UserSchema.findOne({ email });
        if (!user) {
          throw new Error("User not found.");
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
          throw new Error("Invalid password.");
        }
        return user;
      } catch (error) {
        console.error("Error logging in user:", error);
        throw new Error("Failed to log in user.");
      }
    },
    updateUser: async (parent, args) => {
      const { id } = args;
      try {
        await UserSchema.findByIdAndUpdate(id, args);
        const user = await UserSchema.findById(id);
        if (!user) {
          throw new Error("User not found.");
        }
        return user;
      } catch (error) {
        console.error("Error updating user:", error);
        throw new Error("Failed to update user.");
      }
    },
    deleteUser: async (parent, args) => {
      const { email } = args;
      try {
        const user = await UserSchema.findOneAndDelete({email});
        if (!user) {
          throw new Error("User not found.");
        }
        return user;
      } catch (error) {
        console.error("Error deleting user:", error);
        throw new Error("Failed To Delete User !!!.");
      }
    },
    createRestaurant: async (parent, args) => {
      const { rest_name, rest_address, rest_city } = args;
      try {
        const restdata = new RestaurentSchema({
          rest_name,
          rest_address,
          rest_city,
        });
        await restdata.save();
        return restdata
      } catch (error) {
        console.error("Error creating user:", error);
        throw new Error("Failed to create user.");
      }
    },
    updateRestaurant:async (parent,args)=>{
        const { id } = args;
      try {
        await RestaurentSchema.findByIdAndUpdate(id, args);
        const restup = await RestaurentSchema.findById(id);
        if (!restup) {
          throw new Error("Restaurant not found.");
        }
        return restup;
      } catch (error) {
        console.error("Error updating Restaurant:", error);
        throw new Error("Failed to update Restaurant.");
      }
    },
    deleteRestaurant:async (parent,args)=>{
        const { id } = args;
      try {
        const delrest = await RestaurentSchema.findByIdAndDelete(id);
        if (!delrest) {
          throw new Error("Restaurant not found.");
        }
        return delrest;
      } catch (error) {
        console.error("Error deleting Restaurant:", error);
        throw new Error("Failed to delete Restaurant.");
      }
    }
  },
};

module.exports = userResolver;
