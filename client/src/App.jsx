import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider
} from "@apollo/client";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import NavBar from './components/NavBar';
import Getusers from './components/Getusers';
import Createusers from './components/Createusers';
import Deleteusers from './components/Deleteusers';
import Updateusers from './components/Updateusers';

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: "http://localhost:4002/graphql"
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <NavBar />
        <Routes>
          <Route path="/" element={<Getusers/>} />
          <Route path="/create-users" element={<Createusers/>} />
          <Route path="/delete-users" element={<Deleteusers/>} />
          <Route path="/update-users" element={<Updateusers/>} />
        </Routes>
      </Router>
    </ApolloProvider>
  );
};

export default App;
