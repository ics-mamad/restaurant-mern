import { useState } from 'react'
import {useMutation} from '@apollo/client'
import {CREATE_USER} from '../GraphQL/Mutations'

const Createusers = () => {

    const [name,setName] = useState('')
    const [age,setAge] = useState('')
    const [gender,setGender] = useState('')
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')

    const [createUser] = useMutation(CREATE_USER)

    const addUser = async () => {
        try {
          let myage = parseInt(age);
      
          await createUser({
            variables: {
              name: name,
              age: myage,
              gender: gender,
              email: email,
              password: password,
            },
          });
      
          alert("User added successfully!");
        } catch (error) {
          console.error("Error adding user:", error.message);
      
          // You can display a more user-friendly error message to the user
          alert(`Failed to add user. ${error.message}`);
        }
      };
      

  return (
    <div ><b>Create User :</b><br />
      Name : <input type="text" placeholder='name' onChange={(e)=>setName(e.target.value)}/> <br />
      Age : <input type="number" placeholder='age' onChange={(e)=>setAge(e.target.value)}/><br />
      Gender : <input type="text" placeholder='gender' onChange={(e)=>setGender(e.target.value)}/><br />
      Email : <input type="email" placeholder='email' onChange={(e)=>setEmail(e.target.value)}/><br />
      Password : <input type="password" placeholder='password' onChange={(e)=>setPassword(e.target.value)}/><br /><br />
      <button onClick={addUser}>Add User</button>
    </div>
  )
}

export default Createusers
