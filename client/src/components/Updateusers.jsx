import { useState } from "react";
import { useMutation } from "@apollo/client";
import { UPDATE_USER } from "../GraphQL/Mutations";

const Updateusers = () => {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [updateUser, { error }] = useMutation(UPDATE_USER);

  const Update = () => {
    let myage = parseInt(age);

    updateUser({
      variables: {
        id: id,
        ...(name && { name: name }),
        ...(myage && { age: myage }),
        ...(gender && { gender: gender }),
        ...(email && { email: email }),
        ...(password && { password: password }),
      },
    });
    if (error) {
      console.log(error);
    }
    
    setId("");
    setName("");
    setAge("");
    setGender("");
    setEmail("");
    setPassword("");
    console.log(name, age, gender, email, password);
  };

  return (
    <div>
      <b>Update User :</b>
      <br />
      ID :{" "}
      <input
        type="text"
        placeholder="Enter ID To Update"
        onChange={(e) => setId(e.target.value)}
      />
      <br />
      <br />
      Name :{" "}
      <input
        type="text"
        placeholder="enter updated name"
        onChange={(e) => setName(e.target.value)}
      />{" "}
      <br />
      Age :{" "}
      <input
        type="number"
        placeholder="enter updated age"
        onChange={(e) => setAge(e.target.value)}
      />
      <br />
      Gender :{" "}
      <input
        type="text"
        placeholder="enter updated gender"
        onChange={(e) => setGender(e.target.value)}
      />
      <br />
      Email :{" "}
      <input
        type="email"
        placeholder="enter updated email"
        onChange={(e) => setEmail(e.target.value)}
      />
      <br />
      Password :{" "}
      <input
        type="password"
        placeholder="enter updated password"
        onChange={(e) => setPassword(e.target.value)}
      />
      <br />
      <br />
      <button onClick={Update}>Update User</button>
    </div>
  );
};

export default Updateusers;
