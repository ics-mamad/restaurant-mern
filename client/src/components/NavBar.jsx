// import React from 'react';
import { Link } from 'react-router-dom';
import '../components/NavBar.css'; // Import your CSS file

const NavBar = () => {
  return (
    <nav className="navbar">
      <ul className="nav-list">
        <li className="nav-item"><Link to="/">Get Users</Link></li>
        <li className="nav-item"><Link to="/create-users">Create User</Link></li>
        <li className="nav-item"><Link to="/delete-users">Delete User</Link></li>
        <li className="nav-item"><Link to="/update-users">Update User</Link></li>
      </ul>
    </nav>
  );
};

export default NavBar;
